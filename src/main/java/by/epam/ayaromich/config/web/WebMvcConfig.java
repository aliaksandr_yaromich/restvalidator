package by.epam.ayaromich.config.web;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author Aliaksandr_Yaromich
 */
@EnableWebMvc
@Configuration
@ComponentScan(basePackages = "by.epam.ayaromich")
public class WebMvcConfig  {

}
