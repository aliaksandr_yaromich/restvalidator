package by.epam.ayaromich.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

/**
 *
 * @author Aliaksandr_Yaromich
 */
public class User {
    @Size(min = 5, max = 10)
    private String name;
    @Min(18)
    @Max(99)
    private String age;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getAge()
    {
        return age;
    }

    public void setAge(String age)
    {
        this.age = age;
    }
}
