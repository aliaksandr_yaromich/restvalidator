package by.epam.ayaromich.controller;

import javax.validation.Valid;

import by.epam.ayaromich.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Aliaksandr_Yaromich
 */
@RestController
@RequestMapping("/user")
public class UserController { 

    @GetMapping(path = "/{name}/{age}")
    public ResponseEntity<User> echo(@Valid User user) {
        return ResponseEntity.ok(user);
    }

}
